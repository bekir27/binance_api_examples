#!/usr/bin/env python
# coding=utf-8

from binance.client import Client
import time
from threading import Timer
import sched

class Unbuffered(object):
   def __init__(self, stream):
       self.stream = stream
   def write(self, data):
       self.stream.write(data)
       self.stream.flush()
   def writelines(self, datas):
       self.stream.writelines(datas)
       self.stream.flush()
   def __getattr__(self, attr):
       return getattr(self.stream, attr)

import sys
sys.stdout = Unbuffered(sys.stdout)

from get_keys import get_keys
api_public, api_secret = get_keys()
client = Client(api_public, api_secret)

def run_me():
  print("running!")
  i = 0
  while True:
    i += 1
    print(40 * "_" + " " + str(i))
    if (limit_azaldi_mi("NPXSBTC", 300100100)) == 1: break
    if (limit_azaldi_mi("HOTBTC", 500100100)) == 1: break
    time.sleep(20)
  while True:
    alarm_cal()

def limit_azaldi_mi(parite, limit):
  mydata = client.get_orderbook_ticker(symbol=parite)
  if (mydata['bidQty]' < limit or mydata['askQty'] < limit):
    print(parite + " DİKKAT!")
    return 1
  return 0

def alarm_cal():
  import playsound
  playsound.playsound('ses.mp3', True)

run_me()
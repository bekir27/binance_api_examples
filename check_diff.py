#!/usr/bin/env python
# coding=utf-8

from binance.client import Client
import time
import datetime
from threading import Timer
import sched

class Unbuffered(object):
   def __init__(self, stream):
       self.stream = stream
   def write(self, data):
       self.stream.write(data)
       self.stream.flush()
   def writelines(self, datas):
       self.stream.writelines(datas)
       self.stream.flush()
   def __getattr__(self, attr):
       return getattr(self.stream, attr)

import sys
sys.stdout = Unbuffered(sys.stdout)

client = Client("", "")

def run_me():
    print("running!")
    start = datetime.datetime.now()
    i = 0
    while True:
      a = datetime.datetime.now()
      time_gecen = a - start
      print(40 * "_" + " " + str(i) + " ___ " + str(time_gecen.total_seconds()))
      i += 1
      #lazim()
      if(karsilastir("HOT") == 1):
        break
      b = datetime.datetime.now()
      c = b - a
      print("Seconds: " + str(c.total_seconds()))
      if (c.total_seconds() < 1):
        time.sleep(1 - c.total_seconds())

def karsilastir(item):
  data_btc = client.get_orderbook_ticker(symbol = item + "BTC")
  data_eth = client.get_orderbook_ticker(symbol = item + "ETH")
  eth_btc = client.get_orderbook_ticker(symbol="ETHBTC")

  eth_ile_al = float(eth_btc['askPrice']) * float(data_eth['askPrice'])
  eth_ile_sat = float(data_eth['bidPrice']) * float(eth_btc['bidPrice'])
  btc_ile_al = float(data_btc['askPrice'])
  btc_ile_sat = float(data_btc['bidPrice'])

  x = 0

  #eth üzerinden al btc ile sat
  diff = (btc_ile_sat - eth_ile_al) / eth_ile_al * 100
  print("ETH ile alma: " + str(diff))
  if(diff > 0):
    x = 1


  #btc üzerinden al eth ile sat
  diff2 = (eth_ile_sat - btc_ile_al) / btc_ile_al * 100
  print("BTC ile alma: " + str(diff2))
  if(diff2 > 0):
    x = 1

  if(x is 1):
    print("____________________")
    print(data_btc)
    print(data_eth)
    print(eth_btc)
    print("__________")
    print(eth_ile_al)
    print(eth_ile_sat)
    print(btc_ile_al)
    print(btc_ile_sat)
    print("__________")
    print(diff)
    print(diff2)
    if(diff > 0.3 or diff2 > 0.3):
      return 1
  return 0

def lazim():
  mydata = client.get_orderbook_ticker(symbol="ETHBTC")
  eth_fiyat = (float(mydata['bidPrice']) + float(mydata['askPrice'])) / 2
  sonuc = 35 / eth_fiyat
  print(str(sonuc))


run_me()
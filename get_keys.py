#!/usr/bin/env python
# coding=utf-8

def get_keys():
  with open('api_keys.txt', 'r') as api_keys:
    api_public = api_keys.readline()
    api_secret = api_keys.readline()
    return api_public, api_secret